package be.kdg.java2.testendemo.domain;

import java.util.Random;

public class Dice {
    private int value;

    public void throwIt() {
        this.value = new Random().nextInt(1,7);
    }

    public int getValue() {
        return value;
    }
}
