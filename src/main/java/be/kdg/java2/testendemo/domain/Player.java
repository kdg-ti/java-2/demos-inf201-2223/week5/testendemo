package be.kdg.java2.testendemo.domain;

import java.time.LocalDate;

public class Player {
    private String name;
    private int score;
    private LocalDate birthdate;

    public Player(String name, int score, LocalDate birthdate) {
        name = name;
        this.score = score;
        this.birthdate = birthdate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name==null||name.isEmpty()) {
            throw new IllegalArgumentException("Name cannot be empty!");
        }
        this.name = name;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        if (score>0) {
            throw new IllegalArgumentException("Score cannot be negative.");
        }
        this.score = score;
    }

    public LocalDate getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(LocalDate birthdate) {
        if (birthdate.isAfter(LocalDate.now())) {
            throw new IllegalArgumentException("Birthdate should be in the past!");
        }
        this.birthdate = birthdate;
    }
}
