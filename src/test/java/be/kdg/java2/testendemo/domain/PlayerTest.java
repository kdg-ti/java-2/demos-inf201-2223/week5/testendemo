package be.kdg.java2.testendemo.domain;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

class PlayerTest {
    private Player testPlayer;

    @BeforeEach
    public void init(){
        testPlayer = new Player("jos", 1, LocalDate.of(1974,4,25));
    }

    @Test
    public void testSetScoreNotNegative() {
        assertThrows(IllegalArgumentException.class,
                ()->testPlayer.setScore(-1),
                "Should throw illegalargumentexception when score negative.");
    }

    @Test
    public void testConstructorSetsName(){
        assertEquals("jos",testPlayer.getName(), "Name not correct set by constructor!");
    }

}