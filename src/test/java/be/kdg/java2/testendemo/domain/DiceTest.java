package be.kdg.java2.testendemo.domain;

import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class DiceTest {

    @Test
    void throwIt() {
        Dice dice = new Dice();
        for (int i = 0; i < 100; i++) {
            dice.throwIt();
            if (dice.getValue() > 6 || dice.getValue() < 1) {
                fail("Should not throw out of range 1-6");
            }
        }
    }

    @Test
    void throwItThrowsAllValues() {
        Dice dice = new Dice();
        Set<Integer> values = new HashSet<>();
        for (int i = 0; i < 100; i++) {
            dice.throwIt();
            values.add(dice.getValue());
        }
        assertTrue(values.size()==6,"Not all values are thrown");
    }
}